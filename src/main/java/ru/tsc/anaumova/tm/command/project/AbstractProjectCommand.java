package ru.tsc.anaumova.tm.command.project;

import ru.tsc.anaumova.tm.command.AbstractCommand;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

}