package ru.tsc.anaumova.tm.command.task;

import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-index";

    public static final String DESCRIPTION = "Start task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

}